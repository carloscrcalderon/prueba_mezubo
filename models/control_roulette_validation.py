from schema import Schema, And, Use, Optional, SchemaError
from voluptuous import Schema, And, In, Range, MultipleInvalid, Or, Invalid, Optional


def validate_object_roulette(object_to_validate, allow_creation=True):
    schema_ruleta = Schema({
        Optional('apuestas'): list
    })

    schema_apuesta = Schema({
        'usuario_id': int,
        'numero_o_color': And(Or(And(int, Range(min=0, max=36)), And(str, In(['ROJO', 'NEGRO'])))),
        'cantidad': Range(min=0, max=10000)
    })

    try:
        if allow_creation:
            schema_ruleta(object_to_validate)
        else:
            schema_ruleta(object_to_validate, extra=Optional)

        if 'apuestas' in object_to_validate:
            for apuesta in object_to_validate['apuestas']:
                schema_apuesta(apuesta)

        return True
    except MultipleInvalid as e:
        print(e)
        return False
    except Invalid as e:
        print(e)
        return False
