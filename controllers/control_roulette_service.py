import random as rnd
from flask import json, jsonify
from general.general_db import *
from datetime import datetime
from general.general_db import conection, enlace_control_roulette
from models.control_roulette_validation import validate_object_roulette
from bson import ObjectId

class control_roulette:
    def create_control_roulette(documento):
        if not  validate_object_roulette(documento):
            return jsonify({"error": "Invalid object"}), 400
        client = conection()
        collection = enlace_control_roulette(client)
        documento['fecha_de_creacion'] = datetime.now().strftime(
            '%Y-%m-%dT%H:%M:%S.%fZ')  # Agregar fecha de creación al objeto
        result = collection.insert_one(documento)
        return jsonify({"message": "Add successfully", "id": str(result.inserted_id)}), 200

    def open_control_roulette(id_roulette, updated_data):
        client = conection()
        collection = enlace_control_roulette(client)
        existing_key = collection.find_one({"_id": ObjectId(id_roulette)})
        if existing_key:
            if 'estado' not in existing_key or existing_key.get('estado') != 'Abierta':
                if 'estado' not in updated_data:
                    updated_data['estado'] = 'Abierta'
                updated_data['fecha_de_modificación'] = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%fZ')
                if 'estado' in existing_key and existing_key['estado'] == 'Cerrada':
                    print('Ruleta Cerrada')
                    updated_data = {
                        "estado": "Abierta",
                        "fecha_de_creacion": existing_key['fecha_de_creacion'],
                        "apuestas": [],
                        "color_ganador": "",
                        "numero_ganador": "",
                        "resultados": []
                    }

                result = collection.update_one({"_id": ObjectId(id_roulette)}, {"$set": updated_data})

                if result.modified_count > 0:
                    return jsonify({"message": "Updated successfully"}), 200
                else:
                    return jsonify({"message": "No changes made"}), 200
            else:
                return jsonify({"message": "The roulette is already open"}), 200
        else:
            return jsonify({"error": "Roulette key not found"}), 404

    def bet_control_roulette(id_roulette, updated_data, id_user):
        client = conection()
        print(updated_data)
        apuestas = updated_data.get('apuestas', [])
        for apuesta in apuestas:
            apuesta['usuario_id'] = int(id_user)
        print(apuestas)
        if not  validate_object_roulette(updated_data):
            return jsonify({"error": "Invalid object"}), 400
        collection = enlace_control_roulette(client)
        existing_key = collection.find_one({"_id": ObjectId(id_roulette)})
        if existing_key:
            if 'estado' in existing_key and existing_key['estado'] == 'Abierta':
                updated_data['Fecha de Apuesta'] = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%fZ')
                result = collection.update_one({"_id": ObjectId(id_roulette)}, {"$push": {"apuestas": {"$each": apuestas}}})

                if result.modified_count > 0:
                    return jsonify({"message": "Bet successfully"}), 200
                else:
                    return jsonify({"message": "No changes made"}), 200
            else:
                return jsonify({"message": "The roulette is not open"}), 200
        else:
            return jsonify({"error": "Roulette key not found"}), 404

    def close_control_roulette(id_roulette):
        client = conection()
        collection = enlace_control_roulette(client)
        existing_key = collection.find_one({"_id": ObjectId(id_roulette)})

        if existing_key:
            if existing_key['estado'] == 'Cerrada':
                return jsonify({"message": "The roulette is already closed"}), 200

            # Seleccionar número ganador automáticamente
            numero_ganador = rnd.randint(0, 36)
            color_ganador = 'ROJO' if numero_ganador % 2 == 0 else 'NEGRO'

            # Calcular resultados de las apuestas
            apuestas = existing_key.get('apuestas', [])
            resultados = []
            for apuesta in apuestas:
                tipo_apuesta = apuesta['numero_o_color']
                cantidad_apostada = apuesta['cantidad']
                usuario_id = apuesta['usuario_id']
                resultado = {"usuario_id": usuario_id, "tipo_apuesta": tipo_apuesta,
                             "cantidad_apostada": cantidad_apostada}

                if tipo_apuesta == numero_ganador:
                    resultado["ganancia"] = cantidad_apostada * 5
                elif tipo_apuesta == color_ganador:
                    resultado["ganancia"] = cantidad_apostada * 1.8
                else:
                    resultado["ganancia"] = 0

                resultados.append(resultado)

            # Actualizar estado y resultados en la ruleta
            updated_data = {
                "estado": "Cerrada",
                "fecha_de_cierre": datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
                "numero_ganador": numero_ganador,
                "color_ganador": color_ganador,
                "resultados": resultados
            }
            result = collection.update_one({"_id": ObjectId(id_roulette)}, {"$set": updated_data})

            if result.modified_count > 0:
                return jsonify({"message": "Roulette closed successfully","Resultados":updated_data }), 200
            else:
                return jsonify({"message": "No changes made"}), 200
        else:
            return jsonify({"error": "Roulette key not found"}), 404

