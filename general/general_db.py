import os
from flask import request, current_app
from pymongo import MongoClient
import locale
import os
from flask import Flask
import datetime

#locale.setlocale(locale.LC_ALL, 'es_ES.UTF-8')

mongo_uri = os.environ.get('MONGO_URI')
HOST_TYPES = os.environ.get('HOST_TYPES')
API_KEY = os.environ.get('API_KEY')

app = Flask(__name__)



def conection():
    try:
        return MongoClient(mongo_uri)
    except Exception as e:
        return "Error en la base de datos ", e


def enlace_control_roulette(client):
    db = client['roulette-types']
    return db['roulette']

def guardar_log_general():
    log = {
        'Fecha y hora': datetime.datetime.now().strftime('%A, %d de %B de %Y, %I:%M:%S %p'),
        'Ruta': request.path,
        'Metodo': request.method,
        'Direccion_ip': request.remote_addr,
        'Headers': request.headers,
    }

    log_text = str(log) + '\n'

    with open('log.txt', 'a') as file:
        file.write(log_text)
