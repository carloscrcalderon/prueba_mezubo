from logging import info

from flask import jsonify, abort, request
from requests import post, get, put, patch, delete, ConnectTimeout, ReadTimeout

import os

MAX_REQUEST_TIME = os.environ.get('MAX_REQUEST_TIME')


def make_request(url, method, headers=None, body=None, params=None, data=None):
    if headers is None:
        headers = {}
    request_data = {
        "url": url,
        "headers": headers,
        "json": body,
        "params": params,
        "data": data,
        "timeout": MAX_REQUEST_TIME * 60,
    }
    response = None
    try:
        if method == "POST":
            response = post(**request_data)
        elif method == "PUT":
            response = put(**request_data)
        elif method == "GET":
            response = get(**request_data)
        elif method == "PATCH":
            response = patch(**request_data)
        elif method == "DELETE":
            response = delete(**request_data)
    except (ReadTimeout, ConnectTimeout):
        return abort(502)
    if response:
        response.close()
    info(request_data)
    info(
        f"Request duration: {response.elapsed} - "
        f"Status: {response.status_code} - Content: {response.content[:5000]}"
    )
    return response
