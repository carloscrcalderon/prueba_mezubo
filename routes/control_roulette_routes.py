import flask
from flask import request

from controllers.control_roulette_service import control_roulette
from general.general_db import guardar_log_general

roulette_app = flask.Blueprint('roulette_app', __name__)

@roulette_app.route('/api/v1/create_roulette', methods=['POST'])
def createRoulette():
    guardar_log_general()
    return control_roulette.create_control_roulette(request.get_json())


@roulette_app.route('/api/v1/open_roulette/<key>', methods=['PUT'])
def openRoulette(key):
    guardar_log_general()
    return control_roulette.open_control_roulette(key,request.get_json())

@roulette_app.route('/api/v1/bet_roulette/<key>', methods=['PUT'])
def betRoulette(key):
    usuario_id = request.headers.get('id_user')
    guardar_log_general()
    return control_roulette.bet_control_roulette(key, request.get_json(),usuario_id)


@roulette_app.route('/api/v1/close_roulette/<key>', methods=['GET'])
def closeRoulette(key):
    guardar_log_general()
    return control_roulette.close_control_roulette(key)