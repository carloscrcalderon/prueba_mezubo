# Roulette

Este proyecto está desarrollado en Python 3.8. Puede ejecutarse localmente o accederse a través de la siguiente URL: http://137.184.239.237:5005.

## Instalación

Antes de comenzar, asegúrate de tener Python 3.8 instalado en tu sistema.

1. Clona este repositorio en tu máquina local o descarga el archivo ZIP.

```bash
git clone https://gitlab.com/carloscrcalderon/prueba_mezubo.git
```

2. Navega hasta el directorio del proyecto.

```bash
cd prueba_mezubo
```

3. Instala las dependencias utilizando pip y el archivo `requirements.txt`.

```bash
pip install -r requirements.txt
```

## Uso

Una vez completada la instalación, puedes ejecutar la aplicación utilizando el siguiente comando:

```bash
python main.py
```

Esto iniciará el servidor y podrás acceder a la aplicación en tu navegador web a través de `http://localhost:5005`.

## Colección de Postman

Se ha proporcionado una colección de Postman para probar la aplicación tanto en el entorno local como en el servidor. Puedes encontrar los archivos de colección a continuación:

- [Mezubo.postman_collection.json](postman_collection/Mezubo.postman_collection.json) (Prueba local)
- [Server.Mezubo.postman_collection.json](postman_collection/Server.Mezubo.postman_collection.json) (Prueba en servidor)

Sigue los pasos a continuación para utilizar estas colecciones de Postman y realizar pruebas en diferentes entornos.

1. Descarga las colecciones:
   - Haz clic en el enlace [Mezubo.postman_collection.json](postman_collection/Mezubo.postman_collection.json) para descargar la colección de pruebas local.
   - Haz clic en el enlace [Server.Mezubo.postman_collection.json](postman_collection/Server.Mezubo.postman_collection.json) para descargar la colección de pruebas en el servidor.

2. Abre Postman:
   - Si aún no tienes instalado Postman, descárgalo e instálalo desde [https://www.postman.com/downloads/](https://www.postman.com/downloads/).
   - Abre Postman en tu máquina.

3. Importa la colección:
   - En la interfaz de Postman, haz clic en el botón "Importar" en la esquina superior izquierda. Aparecerá una ventana emergente.
   - Haz clic en el botón "Seleccionar archivo" en la ventana emergente y navega hasta la ubicación donde descargaste la colección de Postman.
   - Selecciona el archivo `Mezubo.postman_collection.json` y haz clic en "Abrir" para importarlo. Repite el mismo proceso para importar la colección `Server.Mezubo.postman_collection.json`.

4. Verifica la importación:
   - Después de importar las colecciones, deberían aparecer en el panel izquierdo de Postman bajo la sección "Colecciones".
   - Haz clic en la colección importada para ver los diferentes endpoints (peticiones) disponibles en la colección.

5. Realiza pruebas:
   - Haz clic en una de las peticiones para abrirla en la pestaña principal de Postman.
   - Verifica que la URL de la solicitud coincida con tu entorno (local o servidor).
   - Si es necesario, actualiza los parámetros o el cuerpo de la solicitud según las necesidades.
   - Haz clic en el botón "Enviar" para realizar la solicitud y ver la respuesta del servidor.