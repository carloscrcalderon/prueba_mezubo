from flask import Flask
from routes.control_roulette_routes import roulette_app

app = Flask(__name__)
app.register_blueprint(roulette_app)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5005)
